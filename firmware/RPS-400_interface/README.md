# RPS-400 Interface

Notes about programming the ATmega16U2 microcontroller.

## Fuses

In order to set the fuses, the lock bits must be reset. This is only possible by erasing the chip.
-   Default fuses: (L:5E, H:D9, E:F4)
-   Desired fuses: (L:FF, H:D9, E:F4)

```sh
# Erase the chip (including bootloader)
$ avrdude -c avrisp2 -p m16u2 -e
# Program the fuse bytes
$ avrdude -c avrisp2 -p m16u2 -U lfuse:w:0xff:m -U hfuse:w:0xd9:m -U efuse:w:0xf4:m
```

## Bootloader

[Someone on the internet](http://www.pic-control.com/loading-arduino-bootloader-to-brand-new-atmel-microcontroller/) claims that the ATmega16U2 comes with a USB DFU bootloader preinstalled from the factory. This seems to be untrue, yet this individual lists an ATMEGA16U2 firmware link, which simply points at the `at90usb162-bl-usb-1_0_5.hex` file from the official Microchip [megaAVR DFU USB Bootloaders](http://ww1.microchip.com/downloads/en/DeviceDoc/megaUSB_DFU_Bootloaders.zip).
