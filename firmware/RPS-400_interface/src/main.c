#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Port C
#define PS_ON_ATX_PIN   _BV(PC2)
#define DC_OK_PSU_PIN   _BV(PC4)
#define LED_PIN         _BV(PC5)
#define FAN_PWM_PIN     _BV(PC6)
#define FAN_RPM_PIN     _BV(PC7)
// Port D
#define PS_ON_SPOOF_PIN _BV(PD0)
#define DC_OK_SPOOF_PIN _BV(PD1)
#define RXD_PIN         _BV(PD2)
#define TXD_PIN         _BV(PD3)
#define RTS_PIN         _BV(PD6)
#define CTS_PIN         _BV(PD7)
// USART
#define USART_BAUD_RATE 38400

// USART messages
const char usart_string_psu_on[]  = "PSU on";
const char usart_string_psu_off[] = "PSU off";
// Last received USART value
volatile uint8_t usart_value;
// PSU power status
bool is_psu_on = false;

// Interrupt Service Routine for Receive Complete
ISR(USART_RXC_vect) {
  // Get received data from buffer
  usart_value = UDR1;
}

// Initialize USART peripheral
void USART_Init(uint16_t baud) {
  // Set baud rate
  UBRR1H = baud >> 8;
  UBRR1L = baud;
  // Enable RX complete interrupt, receiver, and transmitter
  UCSR1B = (1 << RXCIE1) | (1 << RXEN1) | (1 << TXEN1);
  // Frame format: 8 data bits, no parity bit, 1 stop bit
  UCSR1C = (1 << USBS1) | (1 << UCSZ11) | (1 << UCSZ10);
}

// Transmit one byte with USART
void USART_Transmit(uint8_t data) {
  // Wait for empty transmit buffer (meaning last byte has been transmitted)
  while(!(UCSR1A & (1 << UDRE1)));
  // Put data into buffer and send it
  UDR1 = data;
}

// Transmit a string with USART
void USART_Transmit_String(char string[]) {
  uint8_t i = 0;
  // Transmit each character until the NUL terminator
  while (string[i] != '\0') {
    USART_Transmit(string[i]);
    if (i == 255) {
      // Catch integer overflow
      return;
    }
    i++;
  }
}

// Receive one byte with USART (unused function, included for completeness)
uint8_t USART_Receive(void) {
  // Wait for data to be received
  while(!(UCSR1A & (1 << RXC1)));
  // Get and return received data from buffer
  return UDR1;
}

void GPIO_Init(void) {
  // Set LED and FAN_PWM pins as outputs on Port C
  DDRC = LED_PIN | FAN_PWM_PIN;
  // Set SPOOF pins as outputs on Port D
  DDRD = PS_ON_SPOOF_PIN | DC_OK_SPOOF_PIN;
}

int main(void) {
  // Initialize USART
  USART_Init(((F_CPU / (USART_BAUD_RATE * 16UL))) - 1);
  // Initialize GPIO
  GPIO_Init();
  // Enable all interrupts
  sei();

  // Turn on the PSU and wait 500ms for the Power Good signal
  PORTD |= PS_ON_SPOOF_PIN;
  _delay_ms(500);

  while (1) {
    // Continuously watch the DC_OK signal coming from the PSU
    if (!is_psu_on && (PINC & DC_OK_PSU_PIN)) {
      is_psu_on = true;
      // Turn on the LED
      PORTC |= LED_PIN;
      // Pass the DC_OK signal through to the motherboard
      PORTD |= DC_OK_SPOOF_PIN;
      // Send a message via USART
      USART_Transmit_String(usart_string_psu_on);
    } else if (is_psu_on && (~PINC & DC_OK_PSU_PIN)) {
      is_psu_on = false;
      // Turn off the LED
      PORTC &= ~LED_PIN;
      // Pass the DC_OK signal through to the motherboard
      PORTD &= ~DC_OK_SPOOF_PIN;
      // Send a message via USART
      USART_Transmit_String(usart_string_psu_off);
    }
  }

  return 0;
}
