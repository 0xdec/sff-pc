EESchema Schematic File Version 4
LIBS:DC-ATX-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DC-ATX:LTC4370 U?
U 1 1 5C5AAB5E
P 5700 4200
AR Path="/5C5AAB5E" Ref="U?"  Part="1" 
AR Path="/5C5AA9A2/5C5AAB5E" Ref="U1"  Part="1" 
F 0 "U1" H 5050 4750 50  0000 C CNN
F 1 "LTC4370" H 6250 4750 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-16-1EP_3x4mm_P0.45mm_EP1.7x3.3mm" H 5700 4200 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/4370f.pdf" H 5700 4200 50  0001 C CNN
	1    5700 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5C5AB0C5
P 6000 5100
AR Path="/5C5AB0C5" Ref="Q?"  Part="1" 
AR Path="/5C5AA9A2/5C5AB0C5" Ref="Q2"  Part="1" 
F 0 "Q2" V 6250 5100 50  0000 C CNN
F 1 "AON7528" V 6341 5100 50  0000 C CNN
F 2 "" H 6200 5200 50  0001 C CNN
F 3 "~" H 6000 5100 50  0001 C CNN
	1    6000 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 3200 6200 3200
Wire Wire Line
	7300 5200 6200 5200
Wire Wire Line
	7300 4200 7400 4200
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5C5AB0BE
P 6000 3300
AR Path="/5C5AB0BE" Ref="Q?"  Part="1" 
AR Path="/5C5AA9A2/5C5AB0BE" Ref="Q1"  Part="1" 
F 0 "Q1" V 6343 3300 50  0000 C CNN
F 1 "AON7528" V 6252 3300 50  0000 C CNN
F 2 "" H 6200 3400 50  0001 C CNN
F 3 "~" H 6000 3300 50  0001 C CNN
	1    6000 3300
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5C5AD064
P 7300 4000
F 0 "R1" H 7359 4046 50  0000 L CNN
F 1 "4m" H 7359 3955 50  0000 L CNN
F 2 "" H 7300 4000 50  0001 C CNN
F 3 "~" H 7300 4000 50  0001 C CNN
	1    7300 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5C5AD092
P 7300 4400
F 0 "R2" H 7359 4446 50  0000 L CNN
F 1 "4m" H 7359 4355 50  0000 L CNN
F 2 "" H 7300 4400 50  0001 C CNN
F 3 "~" H 7300 4400 50  0001 C CNN
	1    7300 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4300 7300 4200
Connection ~ 7300 4200
Wire Wire Line
	7300 4200 7300 4100
Wire Wire Line
	5800 3200 4400 3200
Connection ~ 5800 3200
Wire Wire Line
	5800 5200 4400 5200
Connection ~ 5800 5200
$Comp
L Device:C_Small C3
U 1 1 5C5AE2F4
P 4500 3900
F 0 "C3" H 4592 3946 50  0000 L CNN
F 1 "100n" H 4592 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4500 3900 50  0001 C CNN
F 3 "~" H 4500 3900 50  0001 C CNN
	1    4500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3500 6000 3600
Wire Wire Line
	6000 4900 6000 4800
$Comp
L Device:R_Small R3
U 1 1 5C5AA7CF
P 4700 4300
F 0 "R3" V 4504 4300 50  0000 C CNN
F 1 "47k5" V 4595 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4700 4300 50  0001 C CNN
F 3 "~" H 4700 4300 50  0001 C CNN
	1    4700 4300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5C5AA8FF
P 4700 4600
F 0 "C4" V 4471 4600 50  0000 C CNN
F 1 "29n" V 4562 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4700 4600 50  0001 C CNN
F 3 "~" H 4700 4600 50  0001 C CNN
	1    4700 4600
	0    1    1    0   
$EndComp
Text Notes 2400 4400 0    50   ~ 0
R3=(VFR(MAX)-VFR(MIN))/10uA\n=(480mV-25mV)/10uA\n=~~47.5k
Text Notes 4350 2800 0    50   ~ 0
CPO capacitors (C1,C2) not used because fast gate turn-on not needed at 12V\nOtherwise: C1,C2=Ciss*10\n=2.9nF*10\n=29nF\nWhere Ciss is Q1,Q2 input capacitance
Text Notes 2400 4700 0    50   ~ 0
C4>=Ciss*10 if fast turn-on is not used\nOtherwise: C4>=Ciss*50\nWhere Ciss is Q1,Q2 input capacitance
$Comp
L power:GND #PWR0108
U 1 1 5C5BFED4
P 4500 4700
F 0 "#PWR0108" H 4500 4450 50  0001 C CNN
F 1 "GND" H 4505 4527 50  0000 C CNN
F 2 "" H 4500 4700 50  0001 C CNN
F 3 "" H 4500 4700 50  0001 C CNN
	1    4500 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4000 4500 4000
Wire Wire Line
	4900 3800 4500 3800
Wire Wire Line
	4500 4000 4500 4300
Wire Wire Line
	4500 4300 4600 4300
Connection ~ 4500 4000
Wire Wire Line
	4500 4300 4500 4600
Wire Wire Line
	4500 4600 4600 4600
Connection ~ 4500 4300
Wire Wire Line
	7300 4500 7300 4600
Wire Wire Line
	7300 3200 7300 3800
Wire Wire Line
	6500 3800 7300 3800
Connection ~ 7300 3800
Wire Wire Line
	7300 3800 7300 3900
Wire Wire Line
	6500 4600 7300 4600
Connection ~ 7300 4600
Wire Wire Line
	7300 4600 7300 5200
Wire Wire Line
	4800 4300 4900 4300
Wire Wire Line
	4800 4600 4900 4600
Wire Wire Line
	4500 4700 4500 4600
Connection ~ 4500 4600
Text HLabel 4400 3200 0    50   Input ~ 0
VIN1
Text HLabel 4400 5200 0    50   Input ~ 0
VIN2
Text HLabel 7400 4200 2    50   Output ~ 0
VOUT
Wire Wire Line
	5600 4800 5600 5100
Wire Wire Line
	5800 5100 5800 4800
Wire Wire Line
	5800 5200 5800 5100
Connection ~ 5800 5100
$Comp
L Device:C_Small C2
U 1 1 5C5AD3BD
P 5700 5100
F 0 "C2" V 5471 5100 50  0000 C CNN
F 1 "DNP" V 5562 5100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5700 5100 50  0001 C CNN
F 3 "~" H 5700 5100 50  0001 C CNN
	1    5700 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 3600 5800 3300
Wire Wire Line
	5800 3300 5800 3200
Connection ~ 5800 3300
$Comp
L Device:C_Small C1
U 1 1 5C5AD33C
P 5700 3300
F 0 "C1" V 5471 3300 50  0000 C CNN
F 1 "DNP" V 5562 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5700 3300 50  0001 C CNN
F 3 "~" H 5700 3300 50  0001 C CNN
	1    5700 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3600 5600 3300
$Comp
L Device:R_Small R4
U 1 1 5C5CA069
P 7100 4200
F 0 "R4" V 6904 4200 50  0000 C CNN
F 1 "1k" V 6995 4200 50  0000 C CNN
F 2 "" H 7100 4200 50  0001 C CNN
F 3 "~" H 7100 4200 50  0001 C CNN
	1    7100 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 4200 7300 4200
$Comp
L Device:LED_Small D1
U 1 1 5C5CA569
P 6800 4200
F 0 "D1" H 6800 4435 50  0000 C CNN
F 1 "Red" H 6800 4344 50  0000 C CNN
F 2 "" V 6800 4200 50  0001 C CNN
F 3 "~" V 6800 4200 50  0001 C CNN
	1    6800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4000 6600 4000
Wire Wire Line
	6600 4000 6600 4200
Wire Wire Line
	6600 4200 6700 4200
Wire Wire Line
	6500 4400 6600 4400
Wire Wire Line
	6600 4400 6600 4200
Connection ~ 6600 4200
Wire Wire Line
	6900 4200 7000 4200
Text HLabel 6700 4400 2    50   Output ~ 0
~FAULT
Wire Wire Line
	6700 4400 6600 4400
Connection ~ 6600 4400
Wire Wire Line
	4500 4600 4400 4600
Wire Wire Line
	5400 3600 5400 3300
Wire Wire Line
	5400 3300 4400 3300
Wire Wire Line
	5400 4800 5400 5100
Wire Wire Line
	5400 5100 4400 5100
Text HLabel 4400 3300 0    50   Input ~ 0
~EN1
Text HLabel 4400 5100 0    50   Input ~ 0
~EN2
Text GLabel 4400 4600 0    50   UnSpc ~ 0
GND
$EndSCHEMATC
